var ItemShield = cc.Sprite.extend({
	
	ctor : function(player) {
		this._super();
		this.initWithFile( "res/images/itemShield.png" );
		this.setScale(0.5);
		this.player = player;
	},

	update : function() {
		this.setPosition( this.player.getPosition());
	}
});