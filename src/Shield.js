var Shield = cc.Sprite.extend({

	ctor : function(gameLayer,player) {
		this._super();
		this.player = player;
		this.initWithFile( "res/images/shield.png" );
		this.setScale(0.2);
	},

	update : function(player) {
		this.player = player;
		this.spin();
	},

	spin : function() {
		this.setRotation( this.getRotation() + 15);
	}
});