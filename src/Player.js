var Player = cc.Sprite.extend({

	ctor : function(gameLayer) {
		this._super();
		this.direction = this.getRotation();
		this.turnedLeft = false;
		this.turnedRight = false;
		this.moved = false;
		this.isFired = false;
		this.timeStartFire = null;
		this.gameLayer = gameLayer;
		this.warpList = [2];
		this.nextWarp = 0;
		this.initImage();
		this.life = 10;
		this.warpReady = false;
		this.isAlive = true;
		this.laser = null;
		this.isImmortal = false;
		this.currentTime = null;
		this.rebornTime = null;
		this.isShield = false;
		this.currentBullet = new DefaultBullet(this.gameLayer , this.direction);
		this.ammo = this.currentBullet.initAmmo;
	},

	initImage : function() {
	},

	move : function() {
		var direction = this.getRotation();
		var pos = this.getPosition();
		var newX = pos.x + Math.sin(direction*Math.PI/180.0)*Player.SPEED;
		var newY = pos.y + Math.cos(direction*Math.PI/180.0)*Player.SPEED;
		this.setPosition( cc.p( newX , newY ) );
		this.checkEdge();
	},

	turnRight : function() {
		this.direction = this.getRotation();
		this.setRotation( this.direction + Player.TURNING_SPEED );
	},
	turnLeft : function() {
		this.direction = this.getRotation();
		this.setRotation( this.direction - Player.TURNING_SPEED );
	},

	update : function( dt ) {
		var time = new Date();
		this.currentTime = time.getTime();
		if(this.currentBullet)
		this.direction = this.getRotation();
		if(this.moved) {
			this.move();
		}
		if(this.turnedLeft)
			this.turnLeft();
		if(this.turnedRight)
			this.turnRight();
		if(this.timeStartFire != null) {
			if(this.currentTime - this.timeStartFire >= this.currentBullet.delayPerBullet ) {
				this.isFired = false;
				this.timeStartFire = null;
			}
		}

	},

	checkEdge : function() {
		var pos = this.getPosition();
		if( pos.x < 20 ) {
			this.setPosition( cc.p( 20 , pos.y ) );
		}
		if( pos.y < 20 ) {
			this.setPosition( cc.p( pos.x , 20 ) );
		}
		if( pos.x > screenWidth - 20 ) {
			this.setPosition( cc.p( screenWidth - 20 , pos.y ) );
		}
		if( pos.y > screenHeight - 20 ) {
			this.setPosition( cc.p( pos.x , screenHeight - 20 ) );
		}
		if( pos.x < 20 && pos.y < 20 ) {
			this.setPosition( cc.p( 20 , 20 ) );
		}
		if( pos.x > screenWidth - 20 && pos.y < 20 ) {
			this.setPosition( cc.p( screenWidth - 20 , 20 ) );
		}
		if( pos.x < 20 && pos.y > screenHeight - 20 ) {
			this.setPosition( cc.p( 20 , screenHeight - 20 ) );
		}
		if( pos.x > screenWidth - 20 && pos.y > screenHeight - 20 ) {
			this.setPosition( cc.p( screenWidth - 20 , screenHeight - 20 ) );
		}
	},

	fire : function() {
		if( !(this.isFired) ) {
			var time = new Date();
			this.timeStartFire = time.getTime();
			if( this.currentBullet instanceof DefaultBullet) {
				var currentBullet = new DefaultBullet(this.gameLayer , this.direction);
				var newPosX = this.getPositionX() + Math.sin(this.direction*Math.PI/180.0)*70;
				var newPosY = this.getPositionY() + Math.cos(this.direction*Math.PI/180.0)*70;
				currentBullet.setPosition( cc.p( newPosX , newPosY ) );
				this.currentBullet = currentBullet;
				this.gameLayer.addChild(currentBullet , 1);
				this.gameLayer.arrBullet.push(currentBullet);
				currentBullet.scheduleUpdate();
			}
			else if(this.currentBullet instanceof Blast) {
				var currentBullet = new Blast(this.gameLayer , this.direction);
				var newPosX = this.getPositionX() + Math.sin(this.direction*Math.PI/180.0)*100;
				var newPosY = this.getPositionY() + Math.cos(this.direction*Math.PI/180.0)*100;
				currentBullet.setPosition( cc.p( newPosX , newPosY ) );
				this.currentBullet = currentBullet;
				this.gameLayer.addChild(currentBullet , 1);
				this.gameLayer.arrBullet.push(currentBullet);
				currentBullet.scheduleUpdate();
			}
			else if(this.currentBullet instanceof Laser) {
				var currentBullet = new Laser(this.gameLayer , this.direction);
				var newPosX = this.getPositionX() + Math.sin(this.direction*Math.PI/180.0)*80;
				var newPosY = this.getPositionY() + Math.cos(this.direction*Math.PI/180.0)*80;
				currentBullet.setPosition( cc.p( newPosX , newPosY ) );
				this.currentBullet = currentBullet;
				this.gameLayer.addChild(currentBullet , 1);
				this.gameLayer.arrBullet.push(currentBullet);
				currentBullet.scheduleUpdate();
			}
			else if( this.currentBullet instanceof SpreadBullet) {
				var currentBullet1 = new SpreadBullet(this.gameLayer , this.direction + 30);
				var newPosX1 = this.getPositionX() + Math.sin((this.direction+30)*Math.PI/180.0)*70;
				var newPosY1 = this.getPositionY() + Math.cos((this.direction+30)*Math.PI/180.0)*70;
				currentBullet1.setPosition( cc.p( newPosX1 , newPosY1 ) );
				var currentBullet2 = new SpreadBullet(this.gameLayer , this.direction);
				var newPosX2 = this.getPositionX() + Math.sin(this.direction*Math.PI/180.0)*40;
				var newPosY2 = this.getPositionY() + Math.cos(this.direction*Math.PI/180.0)*40;
				currentBullet2.setPosition( cc.p( newPosX2 , newPosY2 ) );
				var currentBullet3 = new SpreadBullet(this.gameLayer , (this.direction+330)%360 );
				var newPosX3 = this.getPositionX() + Math.sin(((this.direction+330)%360)*Math.PI/180.0)*40;
				var newPosY3 = this.getPositionY() + Math.cos(((this.direction+330)%360)*Math.PI/180.0)*40;
				currentBullet3.setPosition( cc.p( newPosX3 , newPosY3 ) );
				this.currentBullet = currentBullet1;
				this.gameLayer.addChild(currentBullet1 , 1);
				this.gameLayer.addChild(currentBullet2 , 1);
				this.gameLayer.addChild(currentBullet3 , 1);
				this.gameLayer.arrBullet.push(currentBullet1);
				this.gameLayer.arrBullet.push(currentBullet2);
				this.gameLayer.arrBullet.push(currentBullet3);
				currentBullet1.scheduleUpdate();
				currentBullet2.scheduleUpdate();
				currentBullet3.scheduleUpdate();
			}
			this.ammo--;
			this.isFired = true;
		}
	},

	addWarpToWarpList : function( index ) {
		if( this instanceof Player1) {
			this.gameLayer.warpPlayer1[index] = this.warpList[index];
		}
		else if( this instanceof Player2 ) {
			this.gameLayer.warpPlayer2[index] = this.warpList[index];
		}
	},

	fireLaser : function() {
		var laser = new Laser(this.gameLayer , this);
		var newPosX = this.getPositionX() + Math.sin(this.direction*Math.PI/180.0)*40;
		var newPosY = this.getPositionY() + Math.cos(this.direction*Math.PI/180.0)*40;
		laser.setPosition( cc.p( newPosX , newPosY ) );
		this.laser = laser;
		this.gameLayer.addChild(laser , 1);
		laser.scheduleUpdate();
	}
});

Player.SPEED = 8;
Player.TURNING_SPEED = 4;