var Laser = cc.Sprite.extend({
	
	ctor : function( gameLayer , direction ) {
		this._super();
		this.direction = direction;
		this.gameLayer = gameLayer;
		this.delayPerBullet = Laser.DELAY_PER_BULLET;
		this.setRotation( this.direction );
		this.initAmmo = Laser.INIT_AMMO;
		this.initWithFile( "res/images/laser.png" );
	},

	move : function() {
		var direction = this.direction;
		var pos = this.getPosition();		
		var newX = pos.x + Math.sin(direction*Math.PI/180.0)*Laser.SPEED;
		var newY = pos.y + Math.cos(direction*Math.PI/180.0)*Laser.SPEED;
		this.setPosition( cc.p( newX , newY ) );
		this.checkEdge();
	},

	update : function() {
		this.move();
	},

	checkEdge : function() {
		var pos = this.getPosition();
		if( pos.x < 0 || pos.x > screenWidth || pos.y < 0 || pos.y > screenHeight) {
			this.gameLayer.removeChild(this);
		}
	}

});

Laser.SPEED = 80;
Laser.DELAY_PER_BULLET = 200;
Laser.INIT_AMMO = 10;