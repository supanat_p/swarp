var ItemBox = cc.Sprite.extend({

	ctor : function() {
		this._super();
		this.initWithFile( "res/images/itembox.png" );
		this.bullet = this.randomItem();
	},

	update : function() {
	},

	randomItem : function() {
		var random = 1 + Math.floor(Math.random()*100);
		if ( random <= 25 ) {
			return new DefaultBullet();
		}
		else if ( random <= 50 ) {
			return new Laser();
		}
		else if ( random <= 75 ) {
			console.log("SPREAD");
			return new SpreadBullet();
		}
		else {
			return new Blast();
		}
	}
});