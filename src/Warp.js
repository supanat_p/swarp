var Warp = cc.Sprite.extend({
	
	ctor : function() {
		this._super();
		this.setRotation( 0 );
		this.direction = this.getRotation();
		this.initImage();
	},

	initImage : function() {

	},

	spin : function() {
		this.setRotation( this.direction - 10 );
	},

	update : function( dt ) {
		this.direction = this.getRotation();
		this.spin();
	}

});