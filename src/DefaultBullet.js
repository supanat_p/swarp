var DefaultBullet = cc.Sprite.extend({
	
	ctor : function( gameLayer , direction ) {
		this._super();
		this.direction = direction;
		this.gameLayer = gameLayer;
		this.delayPerBullet = DefaultBullet.DELAY_PER_BULLET;
		this.setRotation( this.direction );
		this.initAmmo = DefaultBullet.INIT_AMMO;
		this.initWithFile( "res/images/default_bullet.png" );
	},

	move : function() {
		var direction = this.direction;
		var pos = this.getPosition();		
		var newX = pos.x + Math.sin(direction*Math.PI/180.0)*DefaultBullet.SPEED;
		var newY = pos.y + Math.cos(direction*Math.PI/180.0)*DefaultBullet.SPEED;
		this.setPosition( cc.p( newX , newY ) );
		this.checkEdge();
	},

	update : function() {
		this.move();
	},

	checkEdge : function() {
		var pos = this.getPosition();
		if( pos.x < 0 || pos.x > screenWidth || pos.y < 0 || pos.y > screenHeight) {
			this.gameLayer.removeChild(this);
		}
	}

});

DefaultBullet.SPEED = 10;
DefaultBullet.DELAY_PER_BULLET = 350;
DefaultBullet.INIT_AMMO = 999999;