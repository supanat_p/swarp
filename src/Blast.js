var Blast = cc.Sprite.extend({
	
	ctor : function( gameLayer , direction ) {
		this._super();
		this.direction = direction;
		this.gameLayer = gameLayer;
		this.setRotation( this.direction );
		this.initAmmo = Blast.INIT_AMMO;
		this.delayPerBullet = Blast.DELAY_PER_BULLET;
		this.initWithFile( "res/images/blast.png" );
	},

	move : function() {
		var direction = this.direction;
		var pos = this.getPosition();		
		var newX = pos.x + Math.sin(direction*Math.PI/180.0)*Blast.SPEED;
		var newY = pos.y + Math.cos(direction*Math.PI/180.0)*Blast.SPEED;
		this.setPosition( cc.p( newX , newY ) );
		this.checkEdge();
	},

	update : function() {
		this.move();
	},

	checkEdge : function() {
		var pos = this.getPosition();
		if( pos.x < 0 || pos.x > screenWidth || pos.y < 0 || pos.y > screenHeight) {
			this.gameLayer.removeChild(this);
		}
	}

});

Blast.SPEED = 25;
Blast.DELAY_PER_BULLET = 500;
Blast.INIT_AMMO = 10;