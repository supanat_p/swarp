var Rock = cc.Sprite.extend({
	
	ctor : function(random,gameLayer) {
		this._super();
		this.initWithFile( "res/images/rock.png" );
		this.direction = this.randomDirection(random);
		this.randomCreate();
		this.gameLayer = gameLayer;
		this.setRotation(this.direction);
	},

	randomDirection : function(random) {
		var dir;
		switch(random) {
			case 1 :
				dir = 112 + Math.floor( Math.random()*45 );
				break;
			case 2 :
				dir = 135 + Math.floor( Math.random()*90 );
				break;
			case 3 :
				dir = 202 + Math.floor( Math.random()*45 );
				break;
			case 4 :
				dir = 240 + Math.floor( Math.random()*60 );
				break;
			case 5 :
				dir = 292 + Math.floor( Math.random()*45 );
				break;
			case 6 :
				dir = 315 + Math.floor( Math.random()*90 );
				break;
			case 7 :
				dir = 22 + Math.floor( Math.random()*45 );
				break;
			case 8 :
				dir = 60 + Math.floor( Math.random()*60 );
				break;
		}
		return dir;
	},

	randomCreate : function() {
		var random = 1 + Math.floor( Math.random()*8 );
		this.randomRotationSpeed();
		this.randomSpeed();
	},

	randomSpeed : function() {
		var speed = 4 + Math.floor( Math.random()*4 );
		this.speed = speed;
	},

	randomRotationSpeed : function() {
		var speed = 1 + Math.floor( Math.random()*3 );
		this.rotationSpeed = speed;
	},

	move : function() {
		var direction = this.direction;
		var pos = this.getPosition();		
		var newX = pos.x + Math.sin(direction*Math.PI/180.0)*this.speed;
		var newY = pos.y + Math.cos(direction*Math.PI/180.0)*this.speed;
		this.setPosition( cc.p( newX , newY ) );
		this.checkEdge();
		this.setRotation(this.getRotation() + this.rotationSpeed );
	},

	update : function() {
		this.move();
	},

	checkEdge : function() {
		var pos = this.getPosition();
		if( pos.x < 0 || pos.x > screenWidth || pos.y < 0 || pos.y > screenHeight) {
			this.gameLayer.removeChild(this);
		}
	}

});