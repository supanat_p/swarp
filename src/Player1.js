var Player1 = Player.extend({
	
	initImage : function() {
		this.initWithFile( "res/images/player1.png" );	
	},

	createWarp : function() {
		var position = this.getPosition();
		var newPosX = position.x + Math.sin(this.direction*Math.PI/180.0)*100;
		var newPosY = position.y + Math.cos(this.direction*Math.PI/180.0)*100;
		if( (newPosX >= 40) && (newPosX <= screenWidth-40) && (newPosY >= 40) && (newPosY <= screenHeight-40) ) {
			if(this.nextWarp == 0) {
				if( this.warpList[0] != null) {	
					this.gameLayer.removeChild(this.warpList[0]);
				}
				this.warpList[0] = new Warp1();
				var pos = this.getPosition();
				var newX = pos.x + Math.sin(this.direction*Math.PI/180.0)*100;
				var newY = pos.y + Math.cos(this.direction*Math.PI/180.0)*100;
				this.warpList[0].setPosition( cc.p( newX , newY ) );
				this.gameLayer.addChild( this.warpList[0] , 1 );
				this.addWarpToWarpList(0);
				this.warpList[0].scheduleUpdate();
				this.nextWarp = 1;
			}
			else if( this.nextWarp == 1 ) {
				if( this.warpList[1] != null) {
					this.gameLayer.removeChild( this.warpList[1] );
				}
				this.warpList[1] = new Warp1();
				var pos = this.getPosition();
				var newX = pos.x + Math.sin(this.direction*Math.PI/180.0)*100;
				var newY = pos.y + Math.cos(this.direction*Math.PI/180.0)*100;
				this.warpList[1].setPosition( cc.p( newX , newY ) );
				this.gameLayer.addChild( this.warpList[1] , 1 );
				this.addWarpToWarpList(1);
				this.warpList[1].scheduleUpdate();
				this.nextWarp = 0;
			}
		}
	}

});
