var GameLayer = cc.LayerColor.extend( {

	init : function() {
		this._super( cc.p( 0 , 0 , 0 , 0) );
		this.setPosition( cc.p( 0 , 0 ) );
		this.createBackground();

		this.createPlayer1();
		this.createPlayer2();
		this.addKeyboardHandlers();
		this.arrBullet = [];
		this.warpPlayer1 = [2];
		this.warpPlayer2 = [2];
		this.timeShield = null;
		this.timeDeadPlayer1 = null;
		this.timeDeadPlayer2 = null;
		this.currentTime = null;
		this.arrRock = [];
		this.isHaveItemShield = false;
		var time = new Date();
		this.timeStartGame = time.getTime();
		this.timeRockSpawn = time.getTime();
		this.timeAppendShield = time.getTime();
		this.state = "playing_state";

		this.lifeLabelPlayer1 = cc.LabelTTF.create( '0' , 'Arial' , 25);
		this.lifeLabelPlayer1.setPosition( new cc.p( screenWidth - 75 , screenHeight - 50 ) );
		this.lifeLabelPlayer1.setString( this.player1.life - 1 );
		this.addChild( this.lifeLabelPlayer1 , 4 );

		this.lifeLabelPlayer2 = cc.LabelTTF.create( '0' , 'Arial' , 25);
		this.lifeLabelPlayer2.setPosition( new cc.p( 100 , screenHeight - 50 ) );
		this.lifeLabelPlayer2.setString( this.player2.life - 1 );
		this.addChild( this.lifeLabelPlayer2 , 4 );

		this.imgPlayer1Score = cc.Sprite.create("res/images/player1.png");
		this.imgPlayer1Score.setPosition( cc.p( screenWidth - 100 , screenHeight - 50 ) );
		this.imgPlayer1Score.setScale(0.5);
		this.addChild( this.imgPlayer1Score , 4 );

		this.imgPlayer2Score = cc.Sprite.create("res/images/player2.png");
		this.imgPlayer2Score.setPosition( cc.p( 75 , screenHeight - 50 ) );
		this.imgPlayer2Score.setScale(0.5);
		this.addChild( this.imgPlayer2Score , 4 );

		this.labelAmmoPlayer1 = cc.LabelTTF.create( '0' , 'Arial' , 20);
		this.labelAmmoPlayer1.setPosition( new cc.p(screenWidth - 100 , screenHeight - 100 ) );
		this.labelAmmoPlayer1.setString( "Ammo : " + this.player1.ammo );
		this.addChild( this.labelAmmoPlayer1 , 4);

		this.labelAmmoPlayer2 = cc.LabelTTF.create( '0' , 'Arial' , 20);
		this.labelAmmoPlayer2.setPosition( new cc.p( 100 , screenHeight - 100 ) );
		this.labelAmmoPlayer2.setString( "Ammo : " + this.player2.ammo );
		this.addChild( this.labelAmmoPlayer2 , 4);

		// var audioengine = cc.audioEngine;
  // 		audioengine.playEffect('res/sounds/music2.mp3');

		this.randomPositionItemBox();

		this.scheduleUpdate();

		return true;
	},

	randomPositionItemShield : function() {
		this.isHaveItemShield = true;
		this.itemShield = new ItemShield();
		var randomX = Math.floor(100 + Math.random()*(screenWidth-200));
		var randomY = Math.floor(100 + Math.random()*(screenHeight-200));
		this.itemShield.setPosition( cc.p( randomX , randomY ) );
		this.addChild( this.itemShield , 5 );
	},

	randomPositionItemBox : function() {
		this.itemBox = new ItemBox();
		var randomX = Math.floor(100 + Math.random()*(screenWidth-200));
		var randomY = Math.floor(100 + Math.random()*(screenHeight-200));
		this.itemBox.setPosition( cc.p( randomX , randomY ) );
		this.addChild( this.itemBox , 5 );
	},

	createBackground : function() {
		this.background = cc.Sprite.create("res/images/background2.jpg");
		this.background.setPosition( cc.p( screenWidth/2 , screenHeight/2 ) );
		this.addChild( this.background , 0 );
	},

	createPlayer1 : function() {
		this.player1 = new Player1(this);
		this.player1.setRotation(315);
		this.player1.setPosition( cc.p( screenWidth - 200 , screenHeight/2 ) );
		this.addChild( this.player1 , 2);
		this.player1.scheduleUpdate();
	},	

	createPlayer2 : function() {
		this.player2 = new Player2(this);
		this.player2.setRotation(45);
		this.player2.setPosition( cc.p( 200 , screenHeight/2 ) );
		this.addChild( this.player2 , 2);
		this.player2.scheduleUpdate();
	},

	addKeyboardHandlers : function() {
		var self = this;
		cc.eventManager.addListener({
			event : cc.EventListener.KEYBOARD,
			onKeyPressed : function( keyCode , event ) {
				self.onKeyDown( keyCode , event );
			},
			onKeyReleased : function( keyCode , event ) {
				self.onKeyUp( keyCode , event );
			}
		} , this );
	},

	onKeyDown : function( keyCode , event ) {
		console.log( 'Down : ' + keyCode.toString());
		console.log(this.state);
		if(this.state == "playing_state") {
			if( this.player1.isAlive) {
				if( keyCode == cc.KEY.up ) {
					this.player1.moved = true;
				}
				if( keyCode == cc.KEY.left ) {
					this.player1.turnedLeft = true;
				}
				if( keyCode == cc.KEY.right ) {
					this.player1.turnedRight = true;
				}
				if( keyCode == 191 ) {
					if(this.player1.ammo > 0)
						this.player1.fire();
				}
				if( keyCode == 190 ) {
					this.player1.createWarp();
				}
			}
			if( this.player2.isAlive ) {
				if( keyCode == cc.KEY.w ) {
					this.player2.moved = true;
				}
				if( keyCode == cc.KEY.a ) {
					this.player2.turnedLeft = true;
				}
				if( keyCode == cc.KEY.d ) {
					this.player2.turnedRight = true;
				}
				if( keyCode == cc.KEY.f ) {
					if(this.player2.ammo > 0 )
						this.player2.fire();
				}
				if( keyCode == cc.KEY.g ) {
					this.player2.createWarp();
				}
			}
		}
		if(this.state == "start_state") {
			if( keyCode == cc.KEY.space ) {
				this.state = "playing_state";
			}
		}
		if(this.state == "ending_state") {
			if( keyCode == cc.KEY.space) {
				this.state = "playing_state";
				this.removeChild(this.scene);
				this.player1.life = 10;
				this.player2.life = 10;
			}
		}
	},

	onKeyUp : function( keyCode , event ) {
		console.log( 'Up : ' + keyCode.toString());
		if( this.state == "playing_state") {
			if( keyCode == cc.KEY.up ) {
				this.player1.moved = false;
			}
			if( keyCode == cc.KEY.left ) {
				this.player1.turnedLeft = false;
			}
			if( keyCode == cc.KEY.right ) {
				this.player1.turnedRight = false;
			}
			if(keyCode == cc.KEY.w ) {
				this.player2.moved = false;
			}
			if( keyCode == cc.KEY.a ) {
				this.player2.turnedLeft = false;
			}
			if( keyCode == cc.KEY.d ) {
				this.player2.turnedRight = false;
			} 
		}
	},

	removeBulletOutOfBound : function( bullet , i ) {
		var pos = bullet.getPosition();
		if( pos.x < 0 || pos.x > screenWidth || pos.y < 0 || pos.y > screenHeight) {
			this.arrBullet.splice(i,1);
		}
	},

	removeRockOutOfBound : function( i ) {
		var pos = this.arrRock[i].getPosition();
		if( pos.x < 0 || pos.x > screenWidth || pos.y < 0 || pos.y > screenHeight) {
			this.arrRock.splice(i,1);
		}
	},

	checkPlayer2Death : function( bullet ) {
		if( this.player2.isAlive && !(this.player2.isImmortal) ) {
			if( this.isIntersect( this.player2 , bullet ) ) {
				var audioengine = cc.audioEngine;
    			audioengine.playEffect('res/sounds/explode.mp3');
				this.player2.life--;
				this.removeChild( this.player2 );
				this.removeChild( this.warpPlayer2[0]);
				this.removeChild( this.warpPlayer2[1]);
				this.warpPlayer2[0] = null;
				this.warpPlayer2[1] = null;
				this.player2.warpReady = false;
				this.player2.isAlive = false;
				var time = new Date();
				this.player2.currentBullet = new DefaultBullet(this,this.player2.direction);
				this.timeDeadPlayer2 = time.getTime();
			}
		}
	},

	checkPlayer1Death : function( bullet ) {
		if( this.player1.isAlive && !(this.player1.isImmortal) ) {
			if( this.isIntersect( this.player1 , bullet ) ) {
				var audioengine = cc.audioEngine;
    			audioengine.playEffect('res/sounds/explode.mp3');
				this.player1.life--;
				this.removeChild( this.player1 ); 
				this.removeChild( this.warpPlayer1[0]);
				this.removeChild( this.warpPlayer1[1]);
				this.warpPlayer1[0] = null;
				this.warpPlayer1[1] = null;
				this.player1.warpReady = false;
				this.player1.isAlive = false;
				var time = new Date();
				this.player1.currentBullet = new DefaultBullet(this,this.player1.direction);
				this.timeDeadPlayer1 = time.getTime();
			}
		}
	},

	warp1Bullet : function( bullet ) {
		if( this.player1.warpReady ) {
			for( var i in this.warpPlayer1 ) {
				if( this.checkWarp( this.warpPlayer1[i] , bullet ) ) {
					var newPosX = this.warpPlayer1[1-i].getPositionX() + Math.sin(bullet.direction*Math.PI/180.0)*50;
					var newPosY = this.warpPlayer1[1-i].getPositionY() + Math.cos(bullet.direction*Math.PI/180.0)*50;
					bullet.setPosition( cc.p( newPosX , newPosY ) );
				}
			}
		}
	},

	warp2Bullet : function( bullet ) {
		if( this.player2.warpReady ) {
			for( var i in this.warpPlayer2 ) {
				if( this.checkWarp( this.warpPlayer2[i] , bullet ) ) {
					var newPosX = this.warpPlayer2[1-i].getPositionX() + Math.sin(bullet.direction*Math.PI/180.0)*50;
					var newPosY = this.warpPlayer2[1-i].getPositionY() + Math.cos(bullet.direction*Math.PI/180.0)*50;
					bullet.setPosition( cc.p( newPosX , newPosY ) );
				}
			}
		}
	},

	warp1Player : function() {
		if( this.player1.warpReady ) {
			for( var i=0 ; i<this.warpPlayer1.length ; i++ ) {
				if(this.checkWarp(this.warpPlayer1[i] , this.player1 ) ) {
					var newPosX = this.warpPlayer1[1-i].getPositionX() + Math.sin(this.player1.direction*Math.PI/180.0)*50;
					var newPosY = this.warpPlayer1[1-i].getPositionY() + Math.cos(this.player1.direction*Math.PI/180.0)*50;
					this.player1.setPosition(cc.p( newPosX , newPosY ) );
				}

				if(this.checkWarp(this.warpPlayer1[i] , this.player2 ) ) {
					var newPosX = this.warpPlayer1[1-i].getPositionX() + Math.sin(this.player2.direction*Math.PI/180.0)*50;
					var newPosY = this.warpPlayer1[1-i].getPositionY() + Math.cos(this.player2.direction*Math.PI/180.0)*50;
					this.player2.setPosition(cc.p( newPosX , newPosY ) );
				}
			}
		}
	},

	warp2Player : function() {
		if( this.player2.warpReady ) {
			for( var i=0 ; i<this.warpPlayer2.length ; i++ ) {
				if(this.checkWarp(this.warpPlayer2[i] , this.player1 ) ) {
					var newPosX = this.warpPlayer2[1-i].getPositionX() + Math.sin(this.player1.direction*Math.PI/180.0)*50;
					var newPosY = this.warpPlayer2[1-i].getPositionY() + Math.cos(this.player1.direction*Math.PI/180.0)*50;
					this.player1.setPosition(cc.p( newPosX , newPosY ) );
				}

				if(this.checkWarp(this.warpPlayer2[i] , this.player2 ) ) {
					var newPosX = this.warpPlayer2[1-i].getPositionX() + Math.sin(this.player2.direction*Math.PI/180.0)*50;
					var newPosY = this.warpPlayer2[1-i].getPositionY() + Math.cos(this.player2.direction*Math.PI/180.0)*50;
					this.player2.setPosition(cc.p( newPosX , newPosY ) );
				}
			}
		}
	},

	update : function() {
		
		var time = new Date();
		this.currentTime = time.getTime();

		if(this.player1.isShield) {
			this.shield.setPosition(this.player1.getPosition());
		}

		if(this.player2.isShield) {
			this.shield.setPosition(this.player2.getPosition());
		}

		if( this.player1.isShield || this.player2.isShield ) {
			for(var i=0 ; i<this.arrBullet.length ; i++) {
				if(this.isIntersect(this.arrBullet[i],this.shield)) {
					var newBullet;
					var direction = this.arrBullet[i].getRotation() + 180;
					if( this.arrBullet[i] instanceof DefaultBullet) {
						newBullet = new DefaultBullet(this,direction);
					}
					else if( this.arrBullet[i] instanceof Blast ) {
						newBullet = new Blast(this,direction);
					}
					else if( this.arrBullet[i] instanceof Laser ) {
						newBullet = new Laser(this,direction);
					}
					else if( this.arrBullet[i] instanceof SpreadBullet) {
						newBullet = new SpreadBullet(this,direction);
					}
					var newX = this.shield.getPositionX() + Math.sin(direction*Math.PI/180.0)*100;
					var newY = this.shield.getPositionY() + Math.cos(direction*Math.PI/180.0)*100;
					this.removeChild(this.arrBullet[i]);
					this.arrBullet.splice(i,1);
					newBullet.setPosition( cc.p(newX , newY ) );
					newBullet.setRotation(direction);
					this.arrBullet.push(newBullet);
					this.addChild(newBullet);
					newBullet.scheduleUpdate();
				}
			}
		}

		if(this.player1.life == 0 || this.player2.life == 0) {
			this.scene = cc.Sprite.create("res/images/endstate.jpg");
			this.scene.setPosition( cc.p( screenWidth/2 , screenHeight/2 ) );
			this.addChild( this.scene , 8 );
		}

		if( this.currentTime - this.timeAppendShield >= 10000 ) {
			if( !(this.isHaveItemShield)) {
				this.randomPositionItemShield();
			}
		}

		if( this.currentTime - this.timeShield >= 3000 ) {
			this.removeChild(this.shield);
			this.player1.isShield = false;
			this.player2.isShield = false;
		}

		if( this.isIntersect(this.player1 , this.itemShield ) && this.isHaveItemShield ) {
			this.isHaveItemShield = false;
			this.removeChild(this.itemShield);
			this.shield = new Shield(this.player1);
			this.shield.setPosition(this.player1.getPosition());
			this.addChild(this.shield , 7);
			this.player1.isShield = true;
			this.timeAppendShield = this.currentTime;
			this.timeShield = this.currentTime;
			this.shield.scheduleUpdate(this.player1);
		}

		if( this.isIntersect(this.player2 , this.itemShield ) && this.isHaveItemShield ) {
			this.isHaveItemShield = false;
			this.removeChild(this.itemShield);
			this.shield = new Shield(this.player2);
			this.shield.setPosition(this.player2.getPosition() );
			this.addChild(this.shield , 7);
			this.player2.isShield = true;
			this.timeAppendShield = this.currentTime;
			this.timeShield = this.currentTime;
			this.shield.scheduleUpdate(this.player2);
		}
		
		if(this.warpPlayer1[0] != null && this.warpPlayer1[1] != null) {
			this.player1.warpReady = true;
		}

		if(this.warpPlayer2[0] != null && this.warpPlayer2[1] != null) {
			this.player2.warpReady = true;
		}

		if( this.isIntersect(this.player1 , this.itemBox)) {
			this.player1.currentBullet = this.itemBox.bullet;
			this.player1.ammo = this.player1.currentBullet.initAmmo;
			this.removeChild(this.itemBox);
			this.randomPositionItemBox();
		}

		if( this.isIntersect(this.player2 , this.itemBox)) {
			this.player2.currentBullet = this.itemBox.bullet;
			this.player2.ammo = this.player2.currentBullet.initAmmo;
			this.removeChild(this.itemBox);
			this.randomPositionItemBox();
		}

		if( !( this.player1.isAlive ) ) {
			if( this.currentTime - this.timeDeadPlayer1 >= 3000 ) {
				this.addChild( this.player1 );
				this.player1.scheduleUpdate();
				this.player1.isAlive = true;
				this.player1.rebornTime = this.currentTime;
				this.player1.isImmortal = true;
				this.lifeLabelPlayer1.setString( this.player1.life - 1 );
			}
		}

		if( !( this.player2.isAlive ) ) {
			if( this.currentTime - this.timeDeadPlayer2 >= 3000 ) {
				this.addChild( this.player2 );
				this.player2.scheduleUpdate();
				this.player2.isAlive = true;
				this.player2.rebornTime = this.currentTime;
				this.player2.isImmortal = true;
				this.lifeLabelPlayer2.setString( this.player2.life - 1 );
			}
		}

		if( this.currentTime - this.player1.rebornTime > 2500 )
			this.player1.isImmortal = false;
		if( this.currentTime - this.player2.rebornTime > 2500 )
			this.player2.isImmortal = false;

		for( var i=0 ; i<this.arrBullet.length ; i++ ) {
			this.removeBulletOutOfBound( this.arrBullet[i] , i);
			this.checkPlayer2Death(this.arrBullet[i]);
			this.checkPlayer1Death(this.arrBullet[i]);
			this.warp1Bullet(this.arrBullet[i]);
			this.warp2Bullet(this.arrBullet[i]);
		}

		this.warp1Player();
		this.warp2Player();

		if( this.player1.currentBullet instanceof DefaultBullet)
			this.labelAmmoPlayer1.setString("Ammo : Unlimit");
		else
			this.labelAmmoPlayer1.setString("Ammo : " + this.player1.ammo);
		if( this.player2.currentBullet instanceof DefaultBullet)
			this.labelAmmoPlayer2.setString("Ammo : Unlimit");
		else
			this.labelAmmoPlayer2.setString("Ammo : " + this.player2.ammo);

		if( this.currentTime - this.timeRockSpawn >= 1500 ) {
			this.spawnRock();
			this.timeRockSpawn = this.currentTime;
		} 

		if( this.arrRock != null && this.arrBullet != null ) {
			for( var i=0 ; i < this.arrRock.length ; i++ ) {
				this.removeRockOutOfBound(i);
				for( var j=0 ; j<this.arrBullet.length ; j++ ) {
					if( this.isIntersect( this.arrRock[i] , this.arrBullet[j] ) ) {
						this.removeChild( this.arrBullet[j] );
						this.arrBullet.splice(j,1);
					}
				}
				if( this.isIntersect( this.arrRock[i] , this.player1 ) && this.player1.isAlive && !(this.player1.isImmortal) ) {
					var audioengine = cc.audioEngine;
    				audioengine.playEffect('res/sounds/explode.mp3');
					this.player1.life--;
					this.removeChild( this.player1 ); 
					this.removeChild( this.warpPlayer1[0]);
					this.removeChild( this.warpPlayer1[1]);
					this.warpPlayer1[0] = null;
					this.warpPlayer1[1] = null;
					this.player1.warpReady = false;
					this.player1.isAlive = false;
					var time = new Date();
					this.player1.currentBullet = new DefaultBullet(this,this.player1.direction);
					this.timeDeadPlayer1 = time.getTime();
				}
				if( this.isIntersect( this.arrRock[i] , this.player2 ) && this.player2.isAlive && !(this.player2.isImmortal) ) {
					var audioengine = cc.audioEngine;
    				audioengine.playEffect('res/sounds/explode.mp3');
					this.player2.life--;
					this.removeChild( this.player2 );
					this.removeChild( this.warpPlayer2[0]);
					this.removeChild( this.warpPlayer2[1]);
					this.warpPlayer2[0] = null;
					this.warpPlayer2[1] = null;
					this.player2.warpReady = false;
					this.player2.isAlive = false;
					var time = new Date();
					this.player2.currentBullet = new DefaultBullet(this,this.player2.direction);
					this.timeDeadPlayer2 = time.getTime();
				}
			}
		}
		
	},

	spawnRock : function() {
		if(this.state == "playing_state") {
			var random = 1 + Math.floor( Math.random()*8 );
			var rock = new Rock( random , this );
			switch(random) {
				case 1 :
					rock.setPosition( cc.p( 1 , screenHeight - 1) );
					break;
				case 2 :
					rock.setPosition( cc.p( screenWidth / 2 , screenHeight - 1 ) );
					break;
				case 3 :
					rock.setPosition( cc.p( screenWidth - 1 , screenHeight - 1 ) );
					break;
				case 4 :
					rock.setPosition( cc.p( screenWidth - 1 , screenHeight / 2 ) );
					break;
				case 5 :
					rock.setPosition( cc.p( screenWidth - 1 , 1 ) );
					break;
				case 6 :
					rock.setPosition( cc.p( screenWidth / 2 , 1 ) );
					break;
				case 7 :
					rock.setPosition( cc.p( 1 , 1 ) );
					break;
				case 8 :
					rock.setPosition( cc.p( 1 , screenHeight / 2 ) );
					break;
			}
			this.addChild( rock , 2);
			this.arrRock.push(rock);
			rock.scheduleUpdate();
		}
	},

	isIntersect : function( obj1 , obj2 ) {
		if( obj1 != null && obj2 != null ) {
			var obj1Pos = obj1.getPosition();
			var obj2Pos = obj2.getPosition();
			var distanceX = Math.abs(obj1Pos.x - obj2Pos.x);
			var distanceY = Math.abs(obj1Pos.y - obj2Pos.y);
			if( obj2 instanceof Blast) {
				return ( distanceX < 60 && distanceY < 60 );
			}
			else if( obj2 instanceof Laser) {
				return ( distanceX < 50 && distanceY < 50 );
			}
			else if( obj2 instanceof Shield) {
				return ( distanceX < 60 && distanceY < 60 );
			}
			else 
				return ( distanceX < 23 && distanceY < 23 );
		}
	},

	checkWarp : function( obj1 , obj2 ) {
		if( obj1 != null && obj2 != null ) {
			var obj1Pos = obj1.getPosition();
			var obj2Pos = obj2.getPosition();
			var distanceX = Math.abs(obj1Pos.x - obj2Pos.x);
			var distanceY = Math.abs(obj1Pos.y - obj2Pos.y);
			return ( distanceX < 40 && distanceY < 40 );
		}
	}

});

var StartScene = cc.Scene.extend({
	onEnter : function() {
		this._super();
		var layer = new GameLayer();
		layer.init();
		this.addChild( layer );
	}
});