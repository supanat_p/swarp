var res = {
	background4_jpg : "res/images/background4.jpg",
	background5_jpg : "res/images/background5.jpg",
	background2_jpg : "res/images/background2.jpg",
	player1_png : "res/images/player1.png",
	default_bullet_png : "res/images/default_bullet.png",
	player2_png : "res/images/player2.png",
	warp1_png : "res/images/warp1.png",
	warp2_png : "res/images/warp2.png",
	laser_png : "res/images/laser.png",
	blast_png : "res/images/blast.png",
	rock_png : "res/images/rock.png",
	shield_png : "res/images/shield.png",
	itemShield_png : "res/images/itemShield.png",
	spreadbullet_png : "res/images/spreadbullet.png",
	explode_mp3 : "res/sounds/explode.mp3",
	endstate_jpg : "res/images/endstate.jpg"

};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}