var Player = cc.Sprite.extend({

	ctor : function(gameLayer) {
		this._super();
		this.direction = this.getRotation();
		this.turnedLeft = false;
		this.turnedRight = false;
		this.moved = false;
		this.isFired = false;
		this.timeStartFire = null;
		this.gameLayer = gameLayer;
		this.warpList = [2];
		this.nextWarp = 0;
		this.initImage();
		this.life = 10;
		this.warpReady = false;
		this.isAlive = true;
		this.laser = null;
		this.currentTime = null;
		this.currentBullet = null;
	},

	initImage : function() {
	},

	move : function() {
		var direction = this.getRotation();
		var pos = this.getPosition();
		var newX = pos.x + Math.sin(direction*Math.PI/180.0)*Player.SPEED;
		var newY = pos.y + Math.cos(direction*Math.PI/180.0)*Player.SPEED;
		this.setPosition( cc.p( newX , newY ) );
		this.checkEdge();
	},

	turnRight : function() {
		this.direction = this.getRotation();
		this.setRotation( this.direction + Player.TURNING_SPEED );
	},

	turnLeft : function() {
		this.direction = this.getRotation();
		this.setRotation( this.direction - Player.TURNING_SPEED );
	},

	update : function( dt ) {
		var time = new Date();
		this.currentTime = time.getTime();
		if(this.currentBullet)
		this.direction = this.getRotation();
		if(this.moved)
			this.move();
		if(this.turnedLeft)
			this.turnLeft();
		if(this.turnedRight)
			this.turnRight();
		if(this.timeStartFire != null) {
			if(this.currentTime - this.timeStartFire >= this.currentBullet.delayPerBullet ) {
				this.isFired = false;
				this.timeStartFire = null;
			}
		}

	},

	checkEdge : function() {
		var pos = this.getPosition();
		if( pos.x < 0 ) {
			this.setPosition( cc.p( 0 , pos.y ) );
		}
		if( pos.y < 0 ) {
			this.setPosition( cc.p( pos.x , 0 ) );
		}
		if( pos.x > screenWidth ) {
			this.setPosition( cc.p( screenWidth , pos.y ) );
		}
		if( pos.y > screenHeight ) {
			this.setPosition( cc.p( pos.x , screenHeight ) );
		}
		if( pos.x < 0 && pos.y < 0 ) {
			this.setPosition( cc.p( 0 , 0 ) );
		}
		if( pos.x > screenWidth && pos.y < 0 ) {
			this.setPosition( cc.p( screenWidth , 0 ) );
		}
		if( pos.x < 0 && pos.y > screenHeight ) {
			this.setPosition( cc.p( 0 , screenHeight ) );
		}
		if( pos.x > screenWidth && pos.y > screenHeight ) {
			this.setPosition( cc.p( screenWidth , screenHeight ) );
		}
	},

	fire : function(index) {
		if( !(this.isFired) ) {
			var time = new Date();
			this.timeStartFire = time.getTime();
			if(index == 1) {
				var currentBullet = new DefaultBullet(this.gameLayer , this.direction);
				var newPosX = this.getPositionX() + Math.sin(this.direction*Math.PI/180.0)*40;
				var newPosY = this.getPositionY() + Math.cos(this.direction*Math.PI/180.0)*40;
				currentBullet.setPosition( cc.p( newPosX , newPosY ) );
				this.currentBullet = currentBullet;
				this.gameLayer.addChild(currentBullet , 1);
				this.gameLayer.arrBullet.push(currentBullet);
				currentBullet.scheduleUpdate();
			}
			else if(index == 2) {
				var currentBullet = new Blast(this.gameLayer , this.direction);
				var newPosX = this.getPositionX() + Math.sin(this.direction*Math.PI/180.0)*40;
				var newPosY = this.getPositionY() + Math.cos(this.direction*Math.PI/180.0)*40;
				currentBullet.setPosition( cc.p( newPosX , newPosY ) );
				this.currentBullet = currentBullet;
				this.gameLayer.addChild(currentBullet , 1);
				this.gameLayer.arrBullet.push(currentBullet);
				currentBullet.scheduleUpdate();
			}
			this.isFired = true;
		}
	},

	addWarpToWarpList : function( index ) {
		if( this instanceof Player1) {
			this.gameLayer.warpPlayer1[index] = this.warpList[index];
		}
		else if( this instanceof Player2 ) {
			this.gameLayer.warpPlayer2[index] = this.warpList[index];
		}
	},

	fireLaser : function() {
		var laser = new Laser(this.gameLayer , this);
		var newPosX = this.getPositionX() + Math.sin(this.direction*Math.PI/180.0)*40;
		var newPosY = this.getPositionY() + Math.cos(this.direction*Math.PI/180.0)*40;
		laser.setPosition( cc.p( newPosX , newPosY ) );
		this.laser = laser;
		this.gameLayer.addChild(laser , 1);
		laser.scheduleUpdate();
	}
});

Player.SPEED = 8;
Player.TURNING_SPEED = 4;